﻿using System;
using Autofac;
using IoCXamarinForms.Interfaces;
using IoCXamarinForms.Services;
using IoCXamarinForms.ViewModels;

namespace IoCXamarinForms.ApplicationObjects
{
    public class AppSetup
    {
        public IContainer CreateContainer()
        {
            var containerBuilder = new ContainerBuilder();
            RegisterDependencies(containerBuilder);
            return containerBuilder.Build();
        }

        protected virtual void RegisterDependencies(ContainerBuilder cb)
        {
            cb.RegisterType<CoreHelloFormsService>().As<IHelloFormsService>();
            cb.RegisterType<HomeViewModel>().SingleInstance();
        }
    }
}
