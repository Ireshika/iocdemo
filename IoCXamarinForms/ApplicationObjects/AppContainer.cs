﻿using System;
using Autofac;

namespace IoCXamarinForms.ApplicationObjects
{
    public static class AppContainer
    {
        public static IContainer Container { get; set; }
    }
}
