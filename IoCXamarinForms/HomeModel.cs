﻿using System;
using System.ComponentModel;

namespace IoCXamarinForms
{
    public class HomeModel : INotifyPropertyChanged
    {
        #region Private Variables

        private string _platformMessage { get; set; }

        #endregion

        #region Public Variables

        public String PlatformMessage
        {
            get
            {
                return _platformMessage;
            }
            set
            {
                _platformMessage = value;
                OnPropertyChanged(nameof(PlatformMessage));
            }
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
