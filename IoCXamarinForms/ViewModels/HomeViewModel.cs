﻿using System;
using IoCXamarinForms.Interfaces;

namespace IoCXamarinForms.ViewModels
{
    public class HomeViewModel: IViewModel
    {
        public HomeViewModel(IHelloFormsService formsService)
        {
            homeModel = new HomeModel();
            homeModel.PlatformMessage = formsService.GetHelloFormsText();
        }

        public HomeModel homeModel { get; set; }

    }
}
