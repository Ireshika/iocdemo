﻿using System;
namespace IoCXamarinForms.Interfaces
{
    public interface IHelloFormsService
    {
        string GetHelloFormsText();
    }
}
