﻿using IoCXamarinForms.ApplicationObjects;
using IoCXamarinForms.Views;
using Xamarin.Forms;

namespace IoCXamarinForms
{
    public partial class App : Application
    {
        public App(AppSetup setup)
        {
            InitializeComponent();
            AppContainer.Container = setup.CreateContainer();
            MainPage = new HomePage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
