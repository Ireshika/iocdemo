﻿using System;
using Autofac;
using IoCXamarinForms.ApplicationObjects;
using IoCXamarinForms.Interfaces;
using Xamarin.Forms;

namespace IoCXamarinForms.Views
{
    public class BaseViewPage<T>  : ContentPage where T: IViewModel
    {
        readonly T _viewModel;

        public T ViewModel
        {
            get { return _viewModel; }
        }

        public BaseViewPage()
        {
            using (var scope = AppContainer.Container.BeginLifetimeScope())
            {
                _viewModel = AppContainer.Container.Resolve<T>();
                BindingContext = _viewModel;
            }
        }

    }
}

