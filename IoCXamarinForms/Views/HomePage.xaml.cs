﻿using System;
using System.Collections.Generic;
using IoCXamarinForms.ViewModels;
using Xamarin.Forms;

namespace IoCXamarinForms.Views
{
    public class HomePageBase : BaseViewPage<HomeViewModel> { }

    public partial class HomePage : HomePageBase
    {
        public HomePage()
        {
            InitializeComponent();
        }
    }
}
