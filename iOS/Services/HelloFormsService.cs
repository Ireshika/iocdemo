﻿using System;
using IoCXamarinForms.Interfaces;

namespace IoCXamarinForms.iOS.Services
{
    public class HelloFormsService: IHelloFormsService
    {
        public string GetHelloFormsText()
        {
            return "Hello iOS Forms!";
        }
    }
}
