﻿using System;
using Autofac;
using IoCXamarinForms.ApplicationObjects;
using IoCXamarinForms.Interfaces;
using IoCXamarinForms.iOS.Services;

namespace IoCXamarinForms.iOS
{
    public class SetUp : AppSetup
    {
        protected override void RegisterDependencies(ContainerBuilder cb)
        {
            base.RegisterDependencies(cb);

            cb.RegisterType<HelloFormsService>().As<IHelloFormsService>();
        }

    }
}