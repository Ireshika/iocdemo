﻿using System;
using IoCXamarinForms.Interfaces;

namespace IoCXamarinForms.Droid.Services
{
    public class DroidHelloFormsService : IHelloFormsService
    {
        public string GetHelloFormsText()
        {
            return "Hello from Android!";
        }
    }
}