﻿using System;
using Autofac;
using IoCXamarinForms.ApplicationObjects;
using IoCXamarinForms.Droid.Services;
using IoCXamarinForms.Interfaces;

namespace IoCXamarinForms.Droid
{
    public class SetUp: AppSetup
    {
        protected override void RegisterDependencies(ContainerBuilder cb)
        {
            base.RegisterDependencies(cb);

            cb.RegisterType<DroidHelloFormsService>().As<IHelloFormsService>();
        }
    }
}
